#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:55797036:dfe577e99249442841df9384194ff89f94d8a329; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:51741992:6db74979914eadea13b4d355e77fb707d9c5d2e7 EMMC:/dev/block/bootdevice/by-name/recovery dfe577e99249442841df9384194ff89f94d8a329 55797036 6db74979914eadea13b4d355e77fb707d9c5d2e7:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
